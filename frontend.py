"""
A program that stores orchid collection:
name, when bought, basic care, last bloomed

User can:
view all records
search an entry
add entry
update entry
delete entry
close
"""

from tkinter import *
import backend

def get_selected_row(event):
    try:
        global selected_tuple
        index = list1.curselection()[0]
        selected_tuple = list1.get(index)
        entry1.delete(0, END)
        entry1.insert(END, selected_tuple[1])
        entry2.delete(0, END)
        entry2.insert(END, selected_tuple[2])
        entry3.delete(0, END)
        entry3.insert(END, selected_tuple[3])
        entry4.delete(0, END)
        entry4.insert(END, selected_tuple[4])
    except IndexError:
        pass

def view_command():
    list1.delete(0, END)
    for row in backend.view():
        list1.insert(END, row)

def search_command():
    list1.delete(0, END)
    for row in backend.search(name_text.get(), bought_text.get(), care_text.get(), bloomed_text.get()):
        list1.insert(END, row)

def add_command():
    backend.insert(name_text.get(), bought_text.get(), care_text.get(), bloomed_text.get())
    list1.delete(0, END)
    list1.insert(END, (name_text.get(), bought_text.get(), care_text.get(), bloomed_text.get()))

def delete_command():
    backend.delete(selected_tuple[0])

def update_command():
    backend.update(name_text.get(), bought_text.get(), care_text.get(), bloomed_text.get(), selected_tuple[0])


window = Tk()

window.wm_title('My orchids')

label1 = Label(window, text='Name')
label1.grid(row=0, column=0)

label2 = Label(window, text='Bought')
label2.grid(row=0, column=2)

label3 = Label(window, text='Basic care')
label3.grid(row=1, column=0)

label4 = Label(window, text='Last bloomed')
label4.grid(row=1, column=2)

name_text = StringVar()
entry1 = Entry(window, textvariable=name_text)
entry1.grid(row=0, column=1)

bought_text = StringVar()
entry2 = Entry(window, textvariable=bought_text)
entry2.grid(row=0, column=3)

care_text = StringVar()
entry3 = Entry(window, textvariable=care_text)
entry3.grid(row=1, column=1)

bloomed_text = StringVar()
entry4 = Entry(window, textvariable=bloomed_text)
entry4.grid(row=1, column=3)

list1 = Listbox(window, height=6, width=35)
list1.grid(row=2, column=0, rowspan=8, columnspan=2)

scroll1 = Scrollbar(window)
scroll1.grid(row=2, column=2, rowspan=8)

list1.configure(yscrollcommand=scroll1.set)
scroll1.configure(command=list1.yview)

list1.bind('<<ListboxSelect>>', get_selected_row)

button1 = Button(window, text='View all', width=16, command=view_command)
button1.grid(row=2, column=3)

button2 = Button(window, text='Search entry', width=16, command=search_command)
button2.grid(row=3, column=3)

button3 = Button(window, text='Add entry', width=16, command=add_command)
button3.grid(row=4, column=3)

button4 = Button(window, text='Update selected entry', width=16, command=update_command)
button4.grid(row=5, column=3)

button5 = Button(window, text='Delete selected entry', width=16, command=delete_command)
button5.grid(row=6, column=3)

button6 = Button(window, text='Close', width=16, command=window.destroy)
button6.grid(row=7, column=3)

window.mainloop()