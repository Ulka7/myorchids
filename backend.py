import sqlite3

def connect():
    connection = sqlite3.connect('orchids.db')
    cursor = connection.cursor()
    cursor.execute('CREATE TABLE IF NOT EXISTS orchid (id INTEGER PRIMARY KEY, name text, bought integer, basic_care integer, last_bloomed integer)')
    connection.commit()
    connection.close()

def insert(name, bought, care, bloomed):
    connection = sqlite3.connect('orchids.db')
    cursor = connection.cursor()
    cursor.execute('INSERT INTO orchid VALUES (NULL, ?, ?, ?, ?)', (name, bought, care, bloomed))
    connection.commit()
    connection.close()

def view():
    connection = sqlite3.connect('orchids.db')
    cursor = connection.cursor()
    cursor.execute('SELECT * FROM orchid')
    rows = cursor.fetchall()
    connection.close()
    return rows

def search(name='', bought='', care='', bloomed=''):
    connection = sqlite3.connect('orchids.db')
    cursor = connection.cursor()
    cursor.execute('SELECT * FROM orchid WHERE name=? OR bought=? OR basic_care=? OR last_bloomed=?', (name, bought, care, bloomed))
    rows = cursor.fetchall()
    connection.close()
    return rows

def delete(id):
    connection = sqlite3.connect('orchids.db')
    cursor = connection.cursor()
    cursor.execute('DELETE FROM orchid WHERE id=?', (id,))
    connection.commit()
    connection.close()

def update(name, bought, care, bloomed, id):
    connection = sqlite3.connect('orchids.db')
    cursor = connection.cursor()
    cursor.execute('UPDATE orchid SET name=?, bought=?, basic_care=?, last_bloomed=? WHERE id=?', (name, bought, care, bloomed, id))
    connection.commit()
    connection.close()


connect()
# insert('Phalenopis Mini Mark', 2022, 'dimmed light, mild watering', 2022)
# insert('Dendrobium Fire Bird', 2021, 'strong light, mild watering', 2021)
# insert('Psychopsis', 2021, 'strong light, mild watering', 2021)
# delete(1)
# update(2, 'Dendrobium', 2020, 'Strong light', 2020)
# print(search('Dendrobium'))
# print(view())